﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float maxWalkSpeed = 10.0f;
    public float maxTurnSpeed = 100.0f;
    [SerializeField] private Animator myAnimationController;

    // Update is called once per frame
    void Update()
    {
        // Get the horizontal and vertical axis.
        // By default they are mapped to the arrow keys.
        // The value is in the range -1 to 1
        var walkSpeed = Input.GetAxis("Vertical") * maxWalkSpeed;
        var turnSpeed = Input.GetAxis("Horizontal") * maxTurnSpeed;
        // Move translation along the object's z-axis
        transform.Translate(0, 0, walkSpeed * Time.deltaTime);
        // Rotate around our y-axis
        transform.Rotate(0, turnSpeed * Time.deltaTime, 0);

        if (walkSpeed != 0)
        {
            myAnimationController.SetBool("playMovement", true);
            myAnimationController.SetFloat("animSpeed", 1f);
        }
        else
        {
            myAnimationController.SetBool("playMovement", false);
            myAnimationController.SetFloat("animSpeed", 0f);
        }



    }
}
