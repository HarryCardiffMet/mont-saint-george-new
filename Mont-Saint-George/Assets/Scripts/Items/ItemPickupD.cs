﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickupD : Interactable
{
    public Item item;

    public override void Interact()
    {
        base.Interact();

        PickUp();
    }

    void PickUp()
    {
        Debug.Log("Picking up " + item.name);
        bool wasPickedUp = InventoryD.instance.Add(item);
        
        if (wasPickedUp)
            Destroy(gameObject);
    }
}
