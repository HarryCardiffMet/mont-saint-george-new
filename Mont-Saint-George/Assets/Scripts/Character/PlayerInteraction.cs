﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{

    private GameObject triggeringNPC;
    private bool triggering;

    public GameObject npcText;
    public GameObject particle1;
    void Update()
    {
        if (triggering)
        {
            npcText.SetActive(true);

            if(Input.GetKeyDown(KeyCode.E))
            {
                print("Hello, Welcome to Mont-Saint George! To move the Player, Left-click where you would like to go! To interact with objects Right-click, to equip items you can bring up the inventory by using (I) to equip hit (E). Go speak to the merchant over there...");
                DestroyObject(particle1);

            }
            
            
            

        }
        else
        {
            npcText.SetActive(false); 
        }

    }
    void OnTriggerEnter(Collider other)
    {
       if(other.tag == "NPC")
        {
            triggering = true;
            triggeringNPC = other.gameObject;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "NPC")
        {
            triggering = false;
            triggeringNPC = null;
        }

    }

}